app.title = 'barras';
var receber = [{name:'nome1',value:100,tipo:'cartao'},
            {name:'nome2',value:222,tipo:'cartao'},
            {name:'nome3',value:333,tipo:'cartao'},
            {name:'nome4',value:444,tipo:'cartao'},
            {name:'nome5',value:555,tipo:'convenio'},
        ]
var pagar = [{name:'pagar1',value:666,tipo:'cartao'},
            {name:'paga2',value:553,tipo:'cartao'},
            {name:'pagar3',value:645,tipo:'cartao'},
            {name:'pagar4',value:345,tipo:'cartao'},
            {name:'pagar5',value:763,tipo:'convenio'},
        ]
var serie = [];
for (var i = 0; i < pagar.length; i++) {
var stacko = pagar[i].tipo;
if(!stacko){
    serie[stacko]=stacko
}
    serie[stacko].push({name:pagar[i].name,type:'bar',stack:stacko})
    console.log(serie)
}

option = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data:['serie1','serie2','serie3','serie4','serie5','serie6','serie7','serie8','serie9']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        serie,
        {
            name:'serie1',
            type:'bar',
            data:receber
        },
        {
            name:'serie2',
            type:'bar',
            stack: '广告',
            data:[120, 132, 101, 134, 90, 230, 210]
        },
        {
            name:'serie3',
            type:'bar',
            stack: '广告',
            data:[220, 182, 191, 234, 290, 330, 310]
        },
        {
            name:'serie4',
            type:'bar',
            stack: '广告',
            data:[150, 232, 201, 154, 190, 330, 410]
        },
        {
            name:'serie5',
            type:'bar',
            data:[862, 1018, 964, 1026, 1679, 1600, 1570],
            markLine : {
                lineStyle: {
                    normal: {
                        type: 'dashed'
                    }
                },
                data : [
                    [{type : 'min'}, {type : 'max'}]
                ]
            }
        },
        {
            name:'serie6',
            type:'bar',
            barWidth : 5,
            stack: 'serie5',
            data:[620, 732, 701, 734, 1090, 1130, 1120]
        },
        {
            name:'serie7',
            type:'bar',
            stack: 'serie5',
            data:[120, 132, 101, 134, 290, 230, 220]
        },
        {
            name:'serie8',
            type:'bar',
            stack: 'serie5',
            data:[60, 72, 71, 74, 190, 130, 110]
        },
        {
            name:'serie9',
            type:'bar',
            stack: 'serie5',
            data:[62, 82, 91, 84, 109, 110, 120]
        }
    ]
};
